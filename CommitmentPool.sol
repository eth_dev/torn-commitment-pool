pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./CommitmentPoolFactory.sol"
import "./ERC20Tornado.sol";

contract CommitmentPool is Ownable {

	IVerifier verifier;
	uint32 merkleTreeHeight;
	address operator;

	address token;
	uint256 denomination;
	uint256 depositCount;

	uint256 depositCounter;
	bool locked;
	bool active;
	address pool;

	mapping (address => uint256) deposits;

	event TornadoCreated(address tornadoPool, address token, uint256 denomination);
	event NewDepositCount(uint256 newDepositCount);
	event PoolDeactivated(uint256 blockDeactivated);
	event PoolReactivated(uint256 blockReactivated);

	constructor (IVerifier _verifier, uint32 _merkleTreeHeight, address _operator, address _token, uint256 _denomination, uint256 _depositCount) {
		verifier = _verifier;
		merkleTreeHeight = _merkleTreeHeight;
		operator = _operator;
		token = _token;
		denomination = _denomination;
		depositCount = _depositCount;
		locked = false;
		active = true;
	}
	
	function deposit() {
		require(locked == false, "Pool expired. You can deposit your token directly in Tornado now");
		require(active == true, "Pool has been deactivated")
		require(msg.value == 0, "ETH value must be 0");
		token.transferFrom(msg.sender, address(this), denomination);
		deposits(msg.sender)++;
		depositCounter++;
	}

	function withdraw() {
		require(locked == false, "The pool is locked from withdraws now. Funds can be sent to Tornado with the Mix function");
		require( deposits(msg.sender) >= 0, "You do not have any deposits available for withdraw");
		token.transfer(msg.sender, denomination);
		deposits(msg.sender)--;
		depositCounter--;
	}

	function lockPool() {
		require(depositCounter >= depositCount, "The pool has not met its deposit target, yet");
		ERC20Tornado p = new ERC20Tornado(_verifier, _denomination, _merkleTreeHeight, _operator, _token);
		pool = p;
		locked = true;
		emit TornadoCreated(p, token, denomination);
	}

	function mix() {
		require(locked == true, "Tornado is not yet available for this token at this denomination");
		require( deposits(msg.sender) >= 0, "You do not have any deposits available for mixing");
		token.transfer(pool, denomination);
		deposits(msg.sender)--;
		depositCounter--;
	}

	function getDepositCount() returns(uint256){
		return(depositCounter);
	}

	function getDepositCountOfUser(address _user) returns(uint256){
		uint256 d = deposits(_user);
		return(d);
	}

	function getPoolAddress() returns(address){
		require(locked == true);
		return(pool);
	}

	function updateDepositCount() {
		DepositCount = factory.depositCount;
		emit NewDepositCount(DepositCount);
	}

	function deactivatePool() onlyOwner {
		active = false;
		emit PoolDeactivated(block.number);
	}

	function reactivatePool() onlyOwner {
		active = true;
		emit PoolReactivated(block.number);
	}
}
