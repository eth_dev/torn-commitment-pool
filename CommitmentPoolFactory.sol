pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./CommitmentPool.sol";

contract CommitmentPoolFactory is ownable {

	uint256 depositCount;

	mapping(address => mapping(uint256 => address)) pools;

	event NewCommitmentPool(address commitmentPool, address token, uint256 denomination);
	event NewDepositCount(uint256 depositCount);
	event PoolDeactived(address commitmentPool, address token, uint256 denomination);
	event PoolReactived(address commitmentPool, address token, uint256 denomination);

	function newCommitmentPool(IVerifier _verifier, uint32 _merkleTreeHeight, address _operator, address _token, uint256 _denomination) returns (address){
		require(checkIfPoolExists() == false, "This token commitment pool at this denomination already exists");
		CommitmentPool p = new CommitmentPool(_verifier, _merkleTreeHeight, _operator, _token, _denomination, depositCount);
		emit NewCommitmentPool(p, _token, _denomination);
	}

	function updateDepositCount(uint256 _newDepositCount) onlyOwner {
		depositCount = _newDepositCount;
		emit NewDepositCount(depositCount);
	}

	function checkIfPoolExists(address _token, uint256 _denomination) returns (bool){
		bool _pool;
		if (pools[_token][_denomination] == 0){
			_pool = false;
		} else {
			_pool = true;
		}
		return(_pool);
	}

	function deactivatePool(address _token, uint256 _denomination) onlyOwner {
		address p = pools[_token][_denomination];
		p.deactivatePool();
		emit PoolDeactived(p, _token, _denomination);
	}

	function reactivePool(address _token, uint256 _denomination) onlyOwner {
		address p = pools[_token][_denomination];
		p.reactivatePool();
		emit PoolReactived(p, _token, _denomination);
	}
}
